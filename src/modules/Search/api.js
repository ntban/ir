import { dogs } from '../../constants/mock/images';

export const search = image => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve({ data: dogs });
    }, 1000);
  });
};
